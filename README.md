# example_blog

### ajsTheme

I have created a theme using [bootstrap][7] and it is available on [GitHub][8]

---

### Prerequisites: Hugo and git

* Hugo: [Installing documentation](https://gohugo.io/getting-started/installing/)
* Git: [Getting started installing git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Both of these programs are available for macOS, Linux, and Windows.

### Optional: Create a new site

Once hugo is installed, create a new site by typing the following command into your shell:

```shell
hugo new site example_blog
```

This will create some new directories for use with hugo.

#### Alternative

Alternatively, you can download the example blog from my GitLab. If you do that do not run the command above to create a new site.

### Add a theme to the site

Now that the skeleton structure of the site has been created, here is how to add the same theme that is used by this blog:

```shell
cd example_blog
git init
git submodule add $giturl themes/$themename
```

In powershell the commands should be the same but always press <key>TAB</key> when entering the name of a file or directory for the shell to auto-complete you command.

### Configure the site

The default config file `config.toml` can be deleted. Next create a directory called `config` and then `_default` within.

```shell
rm config.toml
mkdir -p config/_default
```

In powershell:

```powershell
Remove-Item '.\config.toml'
New-Item -Path '.\config'
New-Item -Path '.\config\_default'
```

Within this new directory, create four files to configure the hugo theme:

* `config.toml` : This is the main hugo configuration.
* `menus.toml` : This is the config for english toolbar menu on the site.
* `params.toml` : This is specific configuration for the hugo theme.

#### config.toml

```
./config/_default/config.toml
```

#### menus.toml

```
./config/_default/menus.en.toml
```

#### params.toml

```
./config/_default/params.toml
```

Not in this repo but you can create that file to override the theme configuration

### Add content

In this example, new pages in English are added to the directory `content`. The first file added should be `_index.md`.

```content/posts/_index.md
---
title: "Posts"
date: 2020-10-20
description: All posts
---
```

Create a new post:

```content/posts/hello-world.md
---
title: Hello world!
author: aj
date: 2021-04-10T22:43:20+00:00
categories:
  - Homelab

---
This is my first post. This blog will be focused on hybrid cloud technologies. Stay tuned for more.
```

### Updating theme

A theme for Hugo sites can be a [git submodule][5] that is updated with the following command:

```bash
git submodule update --remote --merge
```

## Test the site

Run the following command to test the site on your local machine. The web server will be available at `localhost` port `1313`.

```bash
hugo server -D
```

You can view the site in your browser:

![hugo](/images/hugo.png)

 [1]: https://www.git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F
 [2]: https://www.markdownguide.org/
 [3]: https://gohugo.io/
 [4]: https://gitlab.com/acaylor/example_blog
 [5]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
 [6]: /posts/building-this-blog/
